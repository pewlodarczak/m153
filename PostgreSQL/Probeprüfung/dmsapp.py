import psycopg2
from rich import print as rprint
from rich.console import Console
from os import system
from config import ConfigReader
import logging

'''
TODO:
'''

console = Console()

class DmsApp(ConfigReader):

    def __init__(self, configfile, section) -> None:
        logging.basicConfig(filename='error.log', level=logging.ERROR)
        super().__init__(configfile='database.ini', section='dms')
        try:
            params = super().config()
            self.conn = psycopg2.connect(**params)
            self.cur = self.conn.cursor()
        except psycopg2.OperationalError as e:
            logging.error(e)
            raise e

    def __repr__(self):
        dsn_parameters = self.conn.get_dsn_parameters()
        return f"Connected to database '{dsn_parameters['dbname']}' on host '{dsn_parameters['host']}' as user '{dsn_parameters['user']}'"

    def create_table(self) -> None:
        sql = """
            CREATE TABLE users (
                USER_ID                       SERIAL PRIMARY KEY,
                first_name                    VARCHAR(255), 
                last_name                     VARCHAR(255),
                email                         VARCHAR(100),
                document_file                 BYTEA 
            );

            CREATE TABLE dms (
                DOC_ID                        SERIAL PRIMARY KEY,
                title                         VARCHAR(255), 
                description                   VARCHAR(255),
                document_file                 BYTEA,
                USER_ID                       INTEGER REFERENCES users(USER_ID) ON DELETE CASCADE
            );
            """

        try:
            self.cur.execute(sql)
            self.conn.commit()
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()

    def drop_table(self) -> None:
        sql = """DROP TABLE users, dms CASCADE; 
            """
        try:
            self.cur.execute(sql)
            self.conn.commit()
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()

    def create_user(self, first_name, last_name, email):
        sql = "INSERT INTO users (first_name, last_name, email) VALUES(%s, %s, %s)"
        try:
            self.cur.execute(sql, (first_name, last_name, email))
            self.conn.commit()
        except psycopg2.Error as e:
            logging.error(e)
            print(e)

    def insert_doc(self, title, description, path_to_file, last_name) -> None:
        sql = """
        INSERT INTO dms (title, description, document_file, user_id)
        SELECT %s, %s, %s, user_id FROM users WHERE last_name = %s;
        """
        try:
            doc = open(path_to_file, 'rb').read()
            self.cur.execute(sql, (title, description, psycopg2.Binary(doc), last_name))
            self.conn.commit()
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()
        except FileNotFoundError as e:
            logging.error(e)

    def delete_user(self, email) -> None:
        sql = "DELETE FROM users WHERE email = %s"
        try:
            self.cur.execute(sql, (email,))
            self.conn.commit()
        except psycopg2.Error as e:
            logging.error(e)
            print(e)

    def delete_several(self, docs) -> None:
        sql = "DELETE FROM dms WHERE title = %s"
        try:
            for d in docs:
                self.cur.execute(sql, (d,))
                self.conn.commit()
        except psycopg2.Error as e:
            logging.error(e)
            print(e)

    def list_docs(self) -> None:
        sql = """
        SELECT title, description from dms;
        """
        try:
            self.cur.execute(sql)
            self.conn.commit()
            rows = self.cur.fetchall()
            for row in rows:
                print(row)
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()

    def __del__(self):
        if hasattr(self, "cur"):
            self.cur.close()
            self.conn.close()

def main() -> None:
    try:
        dmsDAO = DmsApp('database.ini', 'dms')
    except psycopg2.OperationalError as e:
        print(f"Unable to connect to database: {e}")
        logging.error(e)
        print('Exiting')
        exit()

    toDo = ''
    print(dmsDAO)
    
    while toDo != 'q':
        print('What do you want to do:')
        print('c    create table')
        print('d    drop table')
        print('cu   create user')
        print('id   insert document')
        print('del  delete user')
        print('ds   delete several')
        print('ld   list docs')
        print('q    quit')

        toDo = input()
        if toDo == 'c':
            dmsDAO.create_table()
        elif toDo == 'd':
            dmsDAO.drop_table()
        elif toDo == 'cu':
            dmsDAO.create_user("John", 'Do', "do@mail.com")
            system('cls')
        elif toDo == 'id':
            # dmsDAO.insert_doc('Security', 'Windows Security and Hardening', 'doc/Windows Security and Hardening.pdf', 'Do')
            dmsDAO.insert_doc('Sapiens', 'Sapiens_ A Brief History of Humankind', 'doc/Sapiens_ A Brief History of Humankind.epub', 'Do')
        elif toDo == 'del':
            dmsDAO.delete_user('do@mail.com')
        elif toDo == 'ds':
            booklist = ['Security', 'Sapiens']
            dmsDAO.delete_several(booklist)
        elif toDo == 'ld':
            dmsDAO.list_docs()
        elif toDo == 'q':
            print()
            rprint(['Au revoir', '😎'])
            print('👋')
            print()
        else:
            print('wrong command')

if __name__ == '__main__':
    main()
