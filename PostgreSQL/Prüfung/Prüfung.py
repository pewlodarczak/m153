import psycopg2
from rich import print as rprint
from rich.console import Console
from os import system
from config import ConfigReader
import logging, datetime

console = Console()

class CmsApp():

    def __init__(self, configfile, section) -> None:
        self.logger = logging.getLogger('error_logger')
        self.logger.setLevel(logging.ERROR)
        formatter = logging.Formatter('[%(asctime)s] %(levelname)s: %(message)s', '%Y-%m-%d %H:%M:%S')
        file_handler = logging.FileHandler('error.log')
        file_handler.setFormatter(formatter)            
        self.logger.addHandler(file_handler)

        try:
            self.params = ConfigReader(configfile='database.ini', section='cmsdb').config()
            self.conn = psycopg2.connect(**self.params)
            self.cur = self.conn.cursor()
        except psycopg2.OperationalError as e:
            self.logger.error(e)
            rprint(e)
            raise e

    def __repr__(self):
        dsn_parameters = self.conn.get_dsn_parameters()
        return f"Connected to database '{dsn_parameters['dbname']}' on host '{dsn_parameters['host']}' as user '{dsn_parameters['user']}'"

    # Aufg 1
    def create_table(self) -> None:
        sql = """
            CREATE TABLE users (
                USER_ID                       SERIAL PRIMARY KEY,
                first_name                    VARCHAR(255), 
                last_name                     VARCHAR(255),
                email                         VARCHAR(100) UNIQUE,
                phone                         INTEGER
            );

            CREATE TABLE cms (
                DOC_ID                        SERIAL PRIMARY KEY,
                title                         VARCHAR(100), 
                description                   VARCHAR(255),
                content_type                  VARCHAR(50),
                document_file                 BYTEA,
                USER_ID                       INTEGER REFERENCES users(USER_ID) ON DELETE CASCADE
            );
            """

        try:
            self.cur.execute(sql)
            self.conn.commit()
        except psycopg2.Error as e:
            self.logger.error(e)
            rprint(e)
            self.conn.rollback()

    def drop_table(self) -> None:
        sql = """DROP TABLE users, cms CASCADE; 
            """
        try:
            self.cur.execute(sql)
            self.conn.commit()
        except psycopg2.Error as e:
            self.logger.error(e)
            rprint(e)
            self.conn.rollback()

    # Aufg 2
    def create_user(self, first_name, last_name, email, phone) -> None:
        sql = "INSERT INTO users (first_name, last_name, email, phone) VALUES(%s, %s, %s, %s)"
        try:
            self.cur.execute(sql, (first_name, last_name, email, phone))
            self.conn.commit()
        except psycopg2.Error as e:
            self.logger.error(e)
            rprint(e)

    # Aufg 3a
    def check_file_exists(self, path_to_file, email):
        with open(path_to_file, "rb") as file:
            binary_data = file.read()

        check_query = "SELECT COUNT(*) FROM cms WHERE document_file = %s  AND USER_ID = (SELECT user_id FROM users WHERE email = %s)"
        self.cur.execute(check_query, (binary_data, email))
        count = self.cur.fetchone()[0]

        if count > 0:
            return True
        else:
            return False

    # Aufg 3, 4
    def insert_doc(self, title, description, type, path_to_file, email) -> None:
        if self.check_file_exists(path_to_file, email):
            self.logger.error(f"File {path_to_file} already in database")
            return

        sql = """
        INSERT INTO cms (title, description, content_type, document_file, user_id)
        SELECT %s, %s, %s, %s, user_id FROM users WHERE email = %s;
        """
        try:
            doc = open(path_to_file, 'rb').read()
            self.cur.execute(sql, (title, description, type, psycopg2.Binary(doc), email))
            self.conn.commit()
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()
        except FileNotFoundError as e:
            logging.error(e)

    # Aufg 5
    def CopyRecord(self, to_email, title, from_email):
        try:
            self.cur.execute("SELECT COUNT(*) FROM cms WHERE user_id = (SELECT user_id FROM users WHERE email = %s) AND title = %s", (to_email, title,))
            count = self.cur.fetchone()[0]
        except psycopg2.Error as e:
            self.logger.error(e)
            rprint(e)

        if count > 0:
            print('File already present for user')
            return
        else: 
            print('void')

        sql = "INSERT INTO cms (title, description, content_type, document_file, user_id) SELECT title, description, content_type, document_file, (SELECT user_id FROM users WHERE email = %s) FROM cms WHERE title = %s AND user_id = (SELECT user_id FROM users WHERE email = %s)"
        try:
            self.cur.execute(sql, (to_email, title, from_email))
            self.conn.commit()
        except psycopg2.Error as e:
            self.logger.error(e)
            rprint(e)

    # Aufg 6
    def DeleteRecord(self, title, email):
        sql = "DELETE FROM cms WHERE title = %s AND user_id = (SELECT user_id FROM users WHERE email = %s)"

        try:
            self.cur.execute(sql, (email))
            self.conn.commit()
        except psycopg2.Error as e:
            self.logger.error(e)
            rprint(e)


    def __del__(self):
        if hasattr(self, "cur"):
            self.cur.close()
            self.conn.close()

def main():
    dbo = CmsApp('database.ini', 'cmsdb')
    rprint(dbo)
    # dbo.create_table()
    # dbo.create_user('John', 'Do', 'do@mail.com', 123456789)
    # dbo.create_user('Jane', 'Doe', 'jdoe@mail.com', 987654321)
    # dbo.check_file_exists('doc/Sapiens_ A Brief History of Humankind.epub', 'jdoe@mail.com')
    # dbo.insert_doc('Sapiens', 'Sapiens_ A Brief History of Humankind', 'epub', 'doc/Sapiens_ A Brief History of Humankind.epub', 'do@mail.com')
    # dbo.insert_doc('Sapiens', 'Sapiens_ A Brief History of Humankind', 'epub', 'doc/Sapiens_ A Brief History of Humankind.epub', 'jdoe@mail.com')
    # dbo.insert_doc('Homo Deus', 'Homo Deus_ A Brief History of Tomorrow', 'epub', 'doc/Homo Deus_ A Brief History of Tomorrow.epub', 'do@mail.com')
    # dbo.insert_doc('Homo Deus', 'Homo Deus_ A Brief History of Tomorrow', 'epub', 'doc/Homo Deus_ A Brief History of Tomorrow.epub', 'jdoe@mail.com')
    # dbo.insert_doc('Homo Deus', 'Homo Deus_ A Brief History of Tomorrow', 'epub', 'doc/Homo Deus_ A Brief History of Tomorrow.epub', 'jdoe@mail.com')
    # dbo.CopyRecord('jdoe@mail.com', 'Sapiens', 'do@mail.com')
    # dbo.CopyRecord('jdoe@mail.com', 'Homo Deus', 'do@mail.com')
    dbo.CopyRecord('do@mail.com', 'Homo Deus', 'jdoe@mail.com')
    # dbo.DeleteRecord('Sapiens', 'jdoe@mail.com')
    dbo = None

if __name__ == '__main__':
    main()        
