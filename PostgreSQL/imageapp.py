import psycopg2
import dbGUI as dbGUI
from rich import print as rprint
from rich.console import Console
from os import system
from config import ConfigReader
import logging

class SqlExecutionError(Exception):
    pass

'''
TODO:
- read directory, select file from directory
- check if file already in database
- user feedback if operation successful
- custom exception
'''

console = Console()

class ImageApp(ConfigReader):

    def __init__(self, configfile, section) -> None:
        logging.basicConfig(filename='error.log', level=logging.ERROR)
        super().__init__(configfile='database.ini', section='postgresql')
        try:
            params = super().config()
            self.conn = psycopg2.connect(**params)
            self.cur = self.conn.cursor()
        except psycopg2.OperationalError as e:
            logging.error(e)
            raise e

    def __repr__(self):
        dsn_parameters = self.conn.get_dsn_parameters()
        return f"Connected to database '{dsn_parameters['dbname']}' on host '{dsn_parameters['host']}' as user '{dsn_parameters['user']}'"
    
    def create_table(self) -> None:
        sql = """CREATE TABLE images (
                IMG_ID                        SERIAL PRIMARY KEY,
                title                         VARCHAR(255), 
                description                   VARCHAR(255),
                image_file                    BYTEA 
            )"""

        try:
            self.cur.execute(sql)
            self.conn.commit()
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()

    def drop_table(self) -> None:
        print('Drop table')
        sql = """DROP TABLE images; 
            """
        try:
            self.cur.execute(sql)
            self.conn.commit()
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()

    def insert_image(self, title, path_to_file, description) -> None:
        sql = "INSERT INTO images(title,image_file,description) VALUES(%s,%s,%s)"
        try:
            img = open(path_to_file, 'rb').read()
            self.cur.execute(sql, (title, psycopg2.Binary(img), description))
            self.conn.commit()
        except psycopg2.Error as e:
            logging.error(e)
            self.conn.rollback()
        except FileNotFoundError as e:
            logging.error(e)
            print(e)
    
    def read_image(self, title) -> None:
        sql = "SELECT title, description, image_file FROM images WHERE images.title = %s "

        try:
            self.cur.execute(sql, (title,))
            blob = self.cur.fetchone()
            if blob is None:
                print(f"Unable to find {title} in database")    
            open('download/' + blob[0] + '.gif', 'wb').write(blob[2])
        except psycopg2.Error as e:
            self.conn.rollback()
            logging.error(e)
        except Exception as e:
            logging.error(f"Unable to find {title} in database: {e}")

    def delete_image(self, title) -> None:
        sql = "DELETE FROM images WHERE images.title = %s"
        try:
            self.cur.execute(sql, (title,))
            self.conn.commit()
        except psycopg2.Error as e:
            logging.error(e)
            print(e)

    def search_db(self) -> None:
        search_term = input("Enter a search term: ")
        self.cur.execute("SELECT title, description FROM images WHERE LOWER(title) LIKE LOWER(%s) OR LOWER(description) LIKE LOWER(%s)", ('%' + search_term + '%', '%' + search_term + '%'))
        rows = self.cur.fetchall()
        if len(rows) > 0:
            for row in rows:
                print(row)
        else:
            print('\nNothing found\n')

    def __del__(self):
        if hasattr(self, "cur"):
            self.cur.close()
            self.conn.close()

def main() -> None:
    try:
        imgDAO = ImageApp('database.ini', 'postgresql')
    except psycopg2.OperationalError as e:
        print(f"Unable to connect to database: {e}")
        logging.error(e)
        print('Exiting')
        exit()

    toDo = ''
    print(imgDAO)
    num = 0

    # while toDo != '4':
    #     dbGUI.printMenu(num)

    #     toDo = input()
    #     if toDo == 'c':
    #         imgDAO.create_table()
    #     elif toDo == 'd':
    #         imgDAO.drop_table()
    #     elif toDo == '1':
    #         imgDAO.insert_image("Flip", 'img/flip.gif', "Flip")
    #         num = 1
    #         system('cls')
    #     elif toDo == '2':
    #         imgDAO.read_image('Flip')
    #         num = 2
    #         system('cls')
    #     elif toDo == '3':
    #         imgDAO.delete_image('Flip')
    #         num = 3
    #     elif toDo == '4':
    #         print()
    #         #rprint(['Au revoir', '😎'])
    #         print('👋')
    #         #console.print(":thumbs_up: Hasta la vista, baby")
    #         #rprint('Hasta la vista, baby', '👽')
    #         console.print('Hasta la vista, baby', style="blink bold red underline on white")
    #         print()
    #     else:
    #         print('wrong command')

    while toDo != 'q':
        print('What do you want to do:')
        print('c    create table')
        print('d    drop table')
        print('i    insert image')
        print('r    read image')
        print('del  delete image')
        print('s    search')
        print('q    quit')

        toDo = input()
        if toDo == 'c':
            imgDAO.create_table()
        elif toDo == 'd':
            imgDAO.drop_table()
        elif toDo == 'i':
            imgDAO.insert_image("Flip", 'img/flip.gif', "This is a flip file")
            num = 1
            system('cls')
        elif toDo == 'r':
            imgDAO.read_image('Flip')
            num = 2
            # system('cls')
        elif toDo == 'del':
            imgDAO.delete_image('Flip')
        elif toDo == 's':
            imgDAO.search_db()
        elif toDo == 'q':
            print()
            # rprint(['Au revoir', '😎'])
            print('👋')
            # console.print(":thumbs_up: Hasta la vista, baby")
            # rprint('Hasta la vista, baby', '👽')
            console.print('Hasta la vista, baby', style="blink bold red underline on white")
            print()
        else:
            print('wrong command')

if __name__ == '__main__':
    main()
