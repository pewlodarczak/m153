Die Python und pip Version überprüfen

```batch
python --version
pip --version
```
Python muss mindestens Version 3.x sein. <br>
Ein virtuelles Envorinment erstellen und aktivieren:

```batch
mkdir dbdev
cd dbdev
python -m venv .venv
.venv\scripts\activate
```

Den PostgreSQL Treiber und rich Modul installieren:

```batch
pip install psycopg2 rich
```

Das Repository herunterladen:

```batch
git clone -n --depth=1 --filter=tree:0 https://gitlab.com/pewlodarczak/m153.git
cd m153
git sparse-checkout set --no-cone PostgreSQL
git checkout
```

Das Python Script starten:

```batch
cd PostgreSQL
python imageapp.py
```


